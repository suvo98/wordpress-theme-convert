<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'carolinawp');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}|pc,<q$sk81E~o<fxl1#::}(GMq+$Rx/EY{QCN(t+S}le2}>1JG($|lb%bQzin`');
define('SECURE_AUTH_KEY',  'J+/CIafS>u&T.z]|}s/k2eyfUR]wB*M;]v{,C!=rh:L!]AK%uyd 6|in~G^FM;h+');
define('LOGGED_IN_KEY',    'M5X;&%>.DdWTh#axa*=;jC0bD7cli&3*2p42 1)xp>.nKBiG;4Jcm|q]YkXP.AUY');
define('NONCE_KEY',        'y-W*^I0<!Yo izc.` u]Cpkl7bza.ipL*Vx2J~mU+I<h7$x$jWZ/$?nWX]EHyeJf');
define('AUTH_SALT',        'TmGatDc^8waRh/_tEm*pr.S>>AqXxVre[#zCz)Rr9G4U-KnyYCwUQml0z@Mnj)V1');
define('SECURE_AUTH_SALT', '4-&d8+^ayNF!/s3~O{Qg8qP$/26FUnt0lHJiELir.$>lF(KIph58?C05IbD&%^0f');
define('LOGGED_IN_SALT',   '$K;g[B](Lrr{E@;,,G,IbY^bZ7JjcLER0!kg]~ZX)Hh?mx)X[lTnv%b=8Q[|;L =');
define('NONCE_SALT',       '$yIyY(i/c*(fT|eE)~z4UMz,GiwD_?(l12F92>e%^ Rs-5&jl5prQn^s`Jj/)D*i');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
