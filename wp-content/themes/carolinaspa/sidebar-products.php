<aside class="col-lg-4 pt-4 pt-lg-0 product_description">
        <h3 class="text-white text-uppercase text-center mt-5">Description</h3>
        <p class="text-center lead p-3"><?php echo the_field('short_description') ?></p>

        <h3 class="text-white text-uppercase text-center mt-2">Price</h3>
        <p class="display-4 text-center lead p-3">$ <?php echo the_field('price') ?></p>
</aside>